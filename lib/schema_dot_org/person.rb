# frozen_string_literal: true

require 'schema_dot_org'

module SchemaDotOrg
  # Model the Schema.org `Person`.  See http://schema.org/Person
  class Person < Thing
    attr_accessor :family_name, :gender, :works_for

    def _to_json_struct
      super.merge({'familyName' => self.family_name, 'gender' => self.gender, 'worksFor' => self.works_for&.to_json_struct})
    end
  end
end
