# frozen_string_literal: true

require 'schema_dot_org'

# Model the Schema.org `Article > SocialMediaPosting`.  See https://schema.org/SocialMediaPosting
#
module SchemaDotOrg
  class SocialMediaPosting < CreativeWork
  end
end
