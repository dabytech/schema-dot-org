# frozen_string_literal: true

require 'schema_dot_org'
require 'schema_dot_org/thing'


module SchemaDotOrg
  # Model the Schema.org `ListItem`.  See https://schema.org/ListItem
  class ListItem < Thing
    attr_accessor :position, :item

    # validates :position,      type: Integer, presence: true
    #
    # validates :item,         type: Thing, allow_nil: true

    def _to_json_struct
      super.merge({
          'position' => position,
          'item' => item.is_a?(String) ? item : item&.to_json_struct
      })
    end
  end
end
