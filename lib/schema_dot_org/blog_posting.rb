# frozen_string_literal: true

require 'schema_dot_org'

# Model the Schema.org `SocialMediaPosting > BlogPosting`.  See https://schema.org/BlogPosting
#
module SchemaDotOrg
  class BlogPosting < CreativeWork
  end
end
