# frozen_string_literal: true

require 'schema_dot_org'

# Model the Schema.org `Thing > CreativeWork`.  See https://schema.org/WebPage
#
module SchemaDotOrg
  class WebPage < CreativeWork
    attr_accessor :breadcrumb, :main_entity, :primary_image_of_page, :has_part, :significant_link

    def _to_json_struct
      super.merge({
        'breadcrumb' => breadcrumb&.to_json_struct,
        'mainEntity' => main_entity&.to_json_struct,
        'primaryImageOfPage' => primary_image_of_page&.to_json_struct,
        'hasPart' => (has_part.is_a?(Array) ? has_part.map(&:to_json_struct) : has_part&.to_json_struct),
        'significantLink' => (significant_link.is_a?(Array) ? significant_link.map(&:to_json_struct) : significant_link&.to_json_struct)
      })
    end
  end
end
