# frozen_string_literal: true

require 'schema_dot_org'

# Model the Schema.org `Thing > CreativeWork > WebPage`.  See https://schema.org/ProfilePage
#
module SchemaDotOrg
  class ProfilePage < WebPage
  end
end
