# frozen_string_literal: true

require 'schema_dot_org'

# Model the Schema.org `Thing > CreativeWork > WebPage`.  See https://schema.org/CollectionPage
#
module SchemaDotOrg
  class CollectionPage < WebPage

  end
end
